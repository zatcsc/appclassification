package com.vav.services.featureclassification;
import java.util.ArrayList;
import java.util.List;

import uet.mdn.mvav.conjunctions.AppAction;
import jmdn.base.util.string.StrUtil;
import jmdn.method.conjunction.Conjunction;
import jmdn.resource.dictionary.AndroidAppsDictionary;
import jmdn.resource.dictionary.LocationWordsDictionary;
import jmdn.resource.dictionary.OrganizationNameDictionary;
import jmdn.resource.dictionary.ProvinceCityDictionary;
import jmdn.resource.dictionary.StreetNameDictionary;
import jmdn.resource.dictionary.TopVnSongsDictionary;
import jmdn.resource.dictionary.VNSitesDictionary;
import jmdn.resource.dictionary.VnPhoneDictionary;
import jmdn.resource.regex.Regex;


public class AppFeatureGenerator {	
	public static List<String> scanFeatures(String sentence){
		List<String> contextPredicates = new ArrayList<String>();
		List<String> tokens = StrUtil.tokenizeString(sentence.toLowerCase());
		for (int i=0; i< tokens.size(); i++){
			contextPredicates.add(tokens.get(i));
			if ( i < tokens.size() - 1 ){
				contextPredicates.add(tokens.get(i) + ":" + tokens.get(i+1));
			}
			if ( i < tokens.size() - 2 ){
				contextPredicates.add(tokens.get(i) +":" + tokens.get(i+1)+":"+tokens.get(i+2));
			}			
		}
		if (AndroidAppsDictionary.checkWordsInText(sentence)){
			contextPredicates.add("h_oa"); 
		}
		if (VNSitesDictionary.checkWordsInText(sentence) || Regex.containsURL(sentence)){
			contextPredicates.add("h_url");
		}
		if (Regex.containsDate(sentence) || Regex.containsTime(sentence)){
			contextPredicates.add("h_dt");
		}		
		if (LocationWordsDictionary.checkWordsInText(sentence)){
			contextPredicates.add("h_lw");
		}
		if (OrganizationNameDictionary.checkWordsInText(sentence)){
			contextPredicates.add("h_on");
		}
		if (StreetNameDictionary.checkWordsInText(sentence)){
			contextPredicates.add("h_stn");
		}
		if (VnPhoneDictionary.checkWordsInText(sentence)){
			contextPredicates.add("h_cn");
		}		
		if (Regex.containsEmail(sentence)){
			contextPredicates.add("h_em");
		}
		if (TopVnSongsDictionary.checkWordsInText(sentence)){
			contextPredicates.add("h_son");
		}
		if (ProvinceCityDictionary.checkWordsInText(sentence)){
			contextPredicates.add("h_pn");
		}
		return contextPredicates;
	}
	public static List<String> scanFeaturesForTrainingData(String line){
		List<String> contextPredicatesLabel = null;
		List<String> allTokens = StrUtil.tokenizeString(line.toLowerCase());
		int size = allTokens.size();
		if ( size < 2){
			return contextPredicatesLabel;
		}
		String label = allTokens.get(size-1);
		contextPredicatesLabel = scanFeatures(StrUtil.join(allTokens,0,size-2));
		contextPredicatesLabel.add(label);
		return contextPredicatesLabel;
	}
}

