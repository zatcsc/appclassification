package com.vav.services.featureclassification;
import org.kohsuke.args4j.Option;


public class CmdOption {
	@Option(name="-filename", usage="Specify the input data filename")
	public String filename = "";
	@Option(name="-noparts", usage="Specify the number of partitions")
	public int noParts = 5;
}
