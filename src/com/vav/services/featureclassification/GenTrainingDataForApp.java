package com.vav.services.featureclassification;
import java.util.ArrayList;
import java.util.List;

import jmdn.base.struct.collection.CollectionUtil;
import jmdn.base.util.filesystem.FileLoader;
import jmdn.base.util.filesystem.FileSaver;
import jmdn.base.util.string.StrUtil;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;


public class GenTrainingDataForApp {
	public static void main(String[] args){
		CmdOption cmdOption = new CmdOption();
		CmdLineParser parser = new CmdLineParser(cmdOption);
		try{
			if (args.length == 0 ){
				showHelp(parser);
				return;
			}
			parser.parseArgument(args);
			perform(cmdOption);
		} catch(CmdLineException cle){
			System.out.println("Command line error: "+ cle.getMessage());
		} catch (Exception ex){
			System.out.println("Error in main: "+ ex.getMessage());
		}
	}
	public static void perform(CmdOption cmdOption){
		List<String> dataLines = FileLoader.readFile(cmdOption.filename, "UTF8");
		List<String > normalizedDataLines = new ArrayList<String>();
		for (int i=0; i < dataLines.size(); i++){
			String line = StrUtil.normalizeString(dataLines.get(i));
			List<String> tokens = StrUtil.tokenizeString(line,",");
			if (tokens.size() < 2 ){
				continue;
			}
			String sentence = StrUtil.normalizeString(tokens.get(0));
			if (sentence.length() <= 1){
				continue;
			}
			String label = StrUtil.normalizeString(tokens.get(1));
			if (label.length() > 0){
				normalizedDataLines.add(sentence+" "+label);
			}
		}
		List<String> cpsLabelList = new ArrayList<String>();
		for (int i=0; i < normalizedDataLines.size(); i++){
			List<String> cpsLabel =					
					AppFeatureGenerator.scanFeaturesForTrainingData(normalizedDataLines.get(i));
			cpsLabelList.add(StrUtil.join(cpsLabel));
		}
			List<String> cpsLabelList1 = new ArrayList<String>();
			List<String> cpsLabelList2 = new ArrayList<String>();
			CollectionUtil<String> collectionUtil = new CollectionUtil<String>();
			collectionUtil.randomPartition(cpsLabelList, cpsLabelList1, cpsLabelList2, cmdOption.noParts);
			System.out.println("Size of first observation collection:" + cpsLabelList1.size());
			System.out.println("Size of first observation collection:" + cpsLabelList2.size());
			FileSaver.saveListString(cpsLabelList1, "test.tagged","UTF8");
			FileSaver.saveListString(cpsLabelList2, "train.tagged","UTF8");
		
	}
	public static void showHelp(CmdLineParser parser){
		System.out.println("GenTrainingDataForApp [options ...] [arguments ...]");
		parser.printUsage(System.out);
	}
}
