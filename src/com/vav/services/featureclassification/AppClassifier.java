package com.vav.services.featureclassification;
import jmdn.base.util.string.StrUtil;
import jmdn.method.classification.maxent.Classification;

public class AppClassifier {
	private Classification classifier = null;
	public AppClassifier(String modelDirectory){
		classifier = new Classification(modelDirectory);		
	}
	public void init(){
		if (!this.classifier.isInitialized()){
			classifier.init();
		}
	}
	public String classify(String command){
		System.out.println(StrUtil.join(AppFeatureGenerator.scanFeatures(command)));
		return classifier.classify(StrUtil.join(AppFeatureGenerator.scanFeatures(command)));
	}
	public static void main(String[] args){
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();
		System.out.println(appClassifier.classify("dân trí"));		
	}	
}
