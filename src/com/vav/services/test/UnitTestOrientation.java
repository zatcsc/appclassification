package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestOrientation {

	@Test
	public void testOrientation1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tắt tự động xoay màn hình";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testOrientation2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xoay màn hình dọc";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testOrientation3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "khóa xoay màn hình";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testOrientation4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bật chế độ xoay";
		assertEquals("settings" , appClassifier.classify(command));
	}		
	@Test
	public void testOrientation5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tự động xoay màn hình";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testOrientation6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xoay màn hình ngang";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testOrientation7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xoay màn hình";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testOrientation8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xoay ngang";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testOrientation9() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xoay dọc";
		assertEquals("settings" , appClassifier.classify(command));
	}		
	
}
