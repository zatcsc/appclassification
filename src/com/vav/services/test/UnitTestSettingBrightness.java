package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestSettingBrightness {

	@Test
	public void testSettingBrightness1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chỉnh độ sáng cao nhất";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingBrightness2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chỉnh độ sáng màn hình";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingBrightness3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chuyển độ sáng màn hình về 75%";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSettingBrightness4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "đặt màn hình 60% độ sáng";
		assertEquals("settings" , appClassifier.classify(command));
	}		
	@Test
	public void testSettingBrightness5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "thay đổi độ sáng";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingBrightness6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "giảm độ sáng";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingBrightness7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "giảm độ sáng màn hình đến mức thấp nhất";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingBrightness8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "làm màn hình tối đi";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingBrightness9() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "màn hình quá sáng gây đau mắt";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSettingBrightness10() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tăng độ sáng lên 70 phần trăm";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSettingBrightness11() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chỉnh độ sáng màn hình vừa phải";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSettingBrightness12() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chỉnh mức sáng trung bìnhgiảm năm phần trăm độ sáng";
		assertEquals("settings" , appClassifier.classify(command));
	}		
}
