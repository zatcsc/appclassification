package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestSettingVolume {

	@Test
	public void testSettingVolume1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "điều chỉnh âm thanh";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingVolume2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "âm thanh lớn quá";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingVolume3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bật loa nhỏ hơn";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSettingVolume4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "giảm 50% âm lượng";
		assertEquals("settings" , appClassifier.classify(command));
	}		
	@Test
	public void testSettingVolume5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "giảm chuông nhỏ hơn";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingVolume6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "giảm âm thanh loa đi";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingVolume7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "cho nhỏ nhạc đi";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingVolume8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tăng nhạc to hết cỡ lên";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingVolume9() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tăng chuông to lên";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSettingVolume10() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tăng âm lượng";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSettingVolume11() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "giảm âm lượng";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSettingVolume12() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chỉnh âm lượng về mức thấp nhất";
		assertEquals("settings" , appClassifier.classify(command));
	}		
}
