package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestAlarm {

	@Test
	public void testAlarm1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "12h45 nhắc tôi thức giấc";
		assertEquals("alarm" , appClassifier.classify(command));
	}
	@Test
	public void testAlarm2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "báo thức tôi dậy lúc 5 giờ kém 15";
		assertEquals("alarm" , appClassifier.classify(command));
	}
	@Test
	public void testAlarm3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "đánh thức tôi dậy sau 1 tiếng nữa";
		assertEquals("alarm" , appClassifier.classify(command));
	}	
	@Test
	public void testAlarm4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "gọi tôi dậy lúc 6 h sáng mai";
		assertEquals("alarm" , appClassifier.classify(command));
	}		
	@Test
	public void testAlarm5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "hẹn chuông tám giờ ngày mai";
		assertEquals("alarm" , appClassifier.classify(command));
	}
	@Test
	public void testAlarm6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "thiết lập báo thức lúc 6 giờ kém 20";
		assertEquals("alarm" , appClassifier.classify(command));
	}
	@Test
	public void testAlarm7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "đặt giờ báo thức";
		assertEquals("alarm" , appClassifier.classify(command));
	}
	@Test
	public void testAlarm8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "hẹn giờ báo thức lúc 6 giờ hàng ngày";
		assertEquals("alarm" , appClassifier.classify(command));
	}
}
