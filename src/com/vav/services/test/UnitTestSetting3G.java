package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestSetting3G {

	@Test
	public void testSetting3G1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bật 3g";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSetting3G2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở truyền dữ liệu";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSetting3G3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bật dịch vụ 3g";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSetting3G4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bật dữ liệu di động";
		assertEquals("settings" , appClassifier.classify(command));
	}		
	@Test
	public void testSetting3G5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "kết nối dịch vụ 3g";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSetting3G6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "kết nối 3g";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSetting3G7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bật kết nói di động";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSetting3G8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "kết nối 3g";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSetting3G9() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bật 3g";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSetting3G10() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở dữ liệu di động";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSetting3G11() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "ngắt 3g";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSetting3G12() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "3g";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSetting3G13() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tắt 3g";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSetting3G14() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "kết nối mạng internet di động";
		assertEquals("settings" , appClassifier.classify(command));
	}			}
