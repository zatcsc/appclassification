package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestEmail {

	@Test
	public void testEmail1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "email";
		assertEquals("email" , appClassifier.classify(command));
	}
	@Test
	public void testEmail2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "check email";
		assertEquals("email" , appClassifier.classify(command));
	}
	@Test
	public void testEmail3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chuyển tiếp thư ";
		assertEquals("email" , appClassifier.classify(command));
	}	
	@Test
	public void testEmail4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "có thư mới từ vuongyen@vnu.edu.vn không";
		assertEquals("email" , appClassifier.classify(command));
	}		
	@Test
	public void testEmail5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem hộp thư đến";
		assertEquals("email" , appClassifier.classify(command));
	}
	@Test
	public void testEmail6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "gửi email đến vanhop94@hotmail.com";
		assertEquals("email" , appClassifier.classify(command));
	}
	@Test
	public void testEmail7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "gửi thư cho Vũ";
		assertEquals("email" , appClassifier.classify(command));
	}
	@Test
	public void testEmail8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "gửi tới mail txkhanh@gmail.com";
		assertEquals("email" , appClassifier.classify(command));
	}
	@Test
	public void testEmail9() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "info@dantri.com";
		assertEquals("email" , appClassifier.classify(command));
	}	
	@Test
	public void testEmail10() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "kiểm tra có mail mới không";
		assertEquals("email" , appClassifier.classify(command));
	}	
	@Test
	public void testEmail11() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "kiểm tra hòm thư ";
		assertEquals("email" , appClassifier.classify(command));
	}	
	@Test
	public void testEmail12() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "Kiểm tra thư đã gửi";
		assertEquals("email" , appClassifier.classify(command));
	}	
	@Test
	public void testEmail13() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "send email cho khanh";
		assertEquals("email" , appClassifier.classify(command));
	}	
	@Test
	public void testEmail14() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "soạn mail";
		assertEquals("email" , appClassifier.classify(command));
	}		
	@Test
	public void testEmail15() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "viết mail cho hoa";
		assertEquals("email" , appClassifier.classify(command));
	}
	@Test
	public void testEmail16() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem email mới nhất";
		assertEquals("email" , appClassifier.classify(command));
	}		
	@Test
	public void testEmail17() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "vào gmail";
		assertEquals("email" , appClassifier.classify(command));
	}		
	@Test
	public void testEmail18() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở gmail";
		assertEquals("email" , appClassifier.classify(command));
	}	
	@Test
	public void testEmail19() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xóa hộp thư đến";
		assertEquals("email" , appClassifier.classify(command));
	}	
	@Test
	public void testEmail20() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm email của Hồng";
		assertEquals("email" , appClassifier.classify(command));
	}		
	@Test
	public void testEmail21() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "đánh dấu đã đọc tất cả các email";
		assertEquals("email" , appClassifier.classify(command));
	}		
	@Test
	public void testEmail22() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm mail của hợp";
		assertEquals("email" , appClassifier.classify(command));
	}	
	@Test
	public void testEmail23() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "Có email nào từ thầy Hiếu không";
		assertEquals("email" , appClassifier.classify(command));
	}	
	@Test
	public void testEmail24() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xóa email trong hộp thư đã gửi";
		assertEquals("email" , appClassifier.classify(command));
	}	
	@Test
	public void testEmail25() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xóa tất cả mail đã gửi";
		assertEquals("email" , appClassifier.classify(command));
	}			
}
