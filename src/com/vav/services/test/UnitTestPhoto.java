package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestPhoto {

	@Test
	public void testPhoto1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "photo";
		assertEquals("photo" , appClassifier.classify(command));
	}
	@Test
	public void testPhoto2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem ảnh";
		assertEquals("photo" , appClassifier.classify(command));
	}
	@Test
	public void testPhoto3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem lại ảnh vừa chụp xong";
		assertEquals("photo" , appClassifier.classify(command));
	}	
	@Test
	public void testPhoto4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem album ảnh";
		assertEquals("photo" , appClassifier.classify(command));
	}		
	@Test
	public void testPhoto5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem photo";
		assertEquals("photo" , appClassifier.classify(command));
	}
	@Test
	public void testPhoto6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở album ảnh";
		assertEquals("photo" , appClassifier.classify(command));
	}
	@Test
	public void testPhoto7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "duyệt album ảnh";
		assertEquals("photo" , appClassifier.classify(command));
	}
	@Test
	public void testPhoto8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem các ảnh đã chụp";
		assertEquals("photo" , appClassifier.classify(command));
	}
	@Test
	public void testPhoto9() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem ảnh trong máy";
		assertEquals("photo" , appClassifier.classify(command));
	}	
	@Test
	public void testPhoto10() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "album ảnh";
		assertEquals("photo" , appClassifier.classify(command));
	}	
	@Test
	public void testPhoto11() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở thư viện ảnh";
		assertEquals("photo" , appClassifier.classify(command));
	}	
	@Test
	public void testPhoto12() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem tất cả hình ảnh trong bộ sưu tập";
		assertEquals("photo" , appClassifier.classify(command));
	}	
	@Test
	public void testPhoto13() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm ảnh chụp ngày 20/10";
		assertEquals("photo" , appClassifier.classify(command));
	}	
	@Test
	public void testPhoto14() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở album hình đã chụp";
		assertEquals("photo" , appClassifier.classify(command));
	}		
	@Test
	public void testPhoto15() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở bộ sưu tập ảnh";
		assertEquals("photo" , appClassifier.classify(command));
	}
	@Test
	public void testPhoto16() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem ảnh vừa chụp";
		assertEquals("photo" , appClassifier.classify(command));
	}		
	@Test
	public void testPhoto17() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem lại ảnh vừa chụp xong";
		assertEquals("photo" , appClassifier.classify(command));
	}		
	@Test
	public void testPhoto18() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem tất cả hình ảnh trong bộ sưu tập";
		assertEquals("photo" , appClassifier.classify(command));
	}	
	@Test
	public void testPhoto19() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem ảnh";
		assertEquals("photo" , appClassifier.classify(command));
	}	
	@Test
	public void testPhoto20() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem album ảnh";
		assertEquals("photo" , appClassifier.classify(command));
	}		
	@Test
	public void testPhoto21() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem tất cả hình ảnh";
		assertEquals("photo" , appClassifier.classify(command));
	}		
	@Test
	public void testPhoto22() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "duyệt album ảnh";
		assertEquals("photo" , appClassifier.classify(command));
	}	
	@Test
	public void testPhoto23() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở thư viện ảnh";
		assertEquals("photo" , appClassifier.classify(command));
	}	
	@Test
	public void testPhoto24() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở album";
		assertEquals("photo" , appClassifier.classify(command));
	}	
	@Test
	public void testPhoto25() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở bộ sưu tập hình ảnh";
		assertEquals("photo" , appClassifier.classify(command));
	}
	@Test
	public void testPhoto26() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem hình";
		assertEquals("photo" , appClassifier.classify(command));
	}		
	@Test
	public void testPhoto27() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem bộ sưu tập hình ảnh";
		assertEquals("photo" , appClassifier.classify(command));
	}		
	@Test
	public void testPhoto28() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem ảnh";
		assertEquals("photo" , appClassifier.classify(command));
	}		
	@Test
	public void testPhoto29() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "vào bộ sưu tập hình ảnh";
		assertEquals("photo" , appClassifier.classify(command));
	}		
	@Test
	public void testPhoto30() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "duyệt hình ảnh";
		assertEquals("photo" , appClassifier.classify(command));
	}	
	@Test
	public void testPhoto31() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở ảnh";
		assertEquals("photo" , appClassifier.classify(command));
	}		
}
