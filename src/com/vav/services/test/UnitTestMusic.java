package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestMusic {

	@Test
	public void testMusic1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bật nhạc";
		assertEquals("music" , appClassifier.classify(command));
	}
	@Test
	public void testMusic2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "music";
		assertEquals("music" , appClassifier.classify(command));
	}
	@Test
	public void testMusic3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bài hát nghe gần đây nhất";
		assertEquals("music" , appClassifier.classify(command));
	}	
	@Test
	public void testMusic4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bật bài My Love";
		assertEquals("music" , appClassifier.classify(command));
	}		
	@Test
	public void testMusic5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chạy bài hát i know you";
		assertEquals("music" , appClassifier.classify(command));
	}
	@Test
	public void testMusic6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chuyển bài hát";
		assertEquals("music" , appClassifier.classify(command));
	}
	@Test
	public void testMusic7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "dừng nhạc";
		assertEquals("music" , appClassifier.classify(command));
	}
	@Test
	public void testMusic8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở bài hát nơi ấy con tìm về của hồ quang hiếu";
		assertEquals("music" , appClassifier.classify(command));
	}
	@Test
	public void testMusic9() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở trình nghe nhạc";
		assertEquals("music" , appClassifier.classify(command));
	}	
	@Test
	public void testMusic10() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở nhạc";
		assertEquals("music" , appClassifier.classify(command));
	}	
	@Test
	public void testMusic11() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "Mở bài hát mưa của trung quân idol";
		assertEquals("music" , appClassifier.classify(command));
	}	
	@Test
	public void testMusic12() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "nghe nhạc cổ điển	";
		assertEquals("music" , appClassifier.classify(command));
	}	
	@Test
	public void testMusic13() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở ứng dụng chơi nhạc";
		assertEquals("music" , appClassifier.classify(command));
	}	
	@Test
	public void testMusic14() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "phát nhạc";
		assertEquals("music" , appClassifier.classify(command));
	}		
	@Test
	public void testMusic15() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "phát bài mùa thu hà nội của bằng kiều";
		assertEquals("music" , appClassifier.classify(command));
	}
	@Test
	public void testMusic16() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở bài hát mới nhất của Taylor Swift";
		assertEquals("music" , appClassifier.classify(command));
	}		
	@Test
	public void testMusic17() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "phát bài hát love story";
		assertEquals("music" , appClassifier.classify(command));
	}		
	@Test
	public void testMusic18() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chạy bài hát nơi tình yêu bắt đầu";
		assertEquals("music" , appClassifier.classify(command));
	}	
	@Test
	public void testMusic19() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "phát bài em của ngày hôm qua";
		assertEquals("music" , appClassifier.classify(command));
	}	
	@Test
	public void testMusic20() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "không nghe nhạc nữa";
		assertEquals("music" , appClassifier.classify(command));
	}		
	@Test
	public void testMusic21() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "phát album mới nhất của dương hoàng yến";
		assertEquals("music" , appClassifier.classify(command));
	}		
	@Test
	public void testMusic22() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở bài hát bất kì trong album mới nhất của Thùy Chi";
		assertEquals("music" , appClassifier.classify(command));
	}	
	@Test
	public void testMusic23() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở nhạc phim Thần Điêu Đại Hiệp";
		assertEquals("music" , appClassifier.classify(command));
	}	
	@Test
	public void testMusic24() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở nhạc phim bản tình ca mùa đông";
		assertEquals("music" , appClassifier.classify(command));
	}	
	@Test
	public void testMusic25() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở bài hát buồn";
		assertEquals("music" , appClassifier.classify(command));
	}
	@Test
	public void testMusic26() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "nghe nhạc thư giãn";
		assertEquals("music" , appClassifier.classify(command));
	}		
	@Test
	public void testMusic27() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tắt nhạc đi ngủ thôi";
		assertEquals("music" , appClassifier.classify(command));
	}		
	@Test
	public void testMusic28() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm bài hát hay";
		assertEquals("music" , appClassifier.classify(command));
	}		
	@Test
	public void testMusic29() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem danh sách bài hát";
		assertEquals("music" , appClassifier.classify(command));
	}		
	@Test
	public void testMusic30() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở bài hát bất kì";
		assertEquals("music" , appClassifier.classify(command));
	}		
}
