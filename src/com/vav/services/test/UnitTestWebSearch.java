package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestWebSearch {

	@Test
	public void testWebSearch1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm phim chiếu rạp";
		assertEquals("websearch" , appClassifier.classify(command));
	}
	@Test
	public void testWebSearch2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm lịch chiếu phim";
		assertEquals("websearch" , appClassifier.classify(command));
	}
	@Test
	public void testWebSearch3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm thông tin chứng khoán";
		assertEquals("websearch" , appClassifier.classify(command));
	}	
	@Test
	public void testWebSearch4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem tin tức giải trí ngày hôm nay";
		assertEquals("websearch" , appClassifier.classify(command));
	}		
	@Test
	public void testWebSearch5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tối nay có phim gì trên vtv3";
		assertEquals("websearch" , appClassifier.classify(command));
	}
	@Test
	public void testWebSearch6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem thông tin về giá xăng";
		assertEquals("websearch" , appClassifier.classify(command));
	}
	@Test
	public void testWebSearch7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem giá vàng";
		assertEquals("websearch" , appClassifier.classify(command));
	}
	@Test
	public void testWebSearch8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tối nay có trận bóng đá nào";
		assertEquals("websearch" , appClassifier.classify(command));
	}
	@Test
	public void testWebSearch9() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở tìm kiếm";
		assertEquals("websearch" , appClassifier.classify(command));
	}	
	@Test
	public void testWebSearch10() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm kiếm";
		assertEquals("websearch" , appClassifier.classify(command));
	}	
	@Test
	public void testWebSearch11() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm kiếm thông tin tuyển sinh mới";
		assertEquals("websearch" , appClassifier.classify(command));
	}	
	@Test
	public void testWebSearch12() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm kiếm google";
		assertEquals("websearch" , appClassifier.classify(command));
	}	
	@Test
	public void testWebSearch13() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm kiếm thông tin trên mạng";
		assertEquals("websearch" , appClassifier.classify(command));
	}	
	@Test
	public void testWebSearch14() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "Cách săn học bổng như thế nào";
		assertEquals("websearch" , appClassifier.classify(command));
	}		
	@Test
	public void testWebSearch15() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "Ngày thi đại học năm nay là ngày nào";
		assertEquals("websearch" , appClassifier.classify(command));
	}	
	@Test
	public void testWebSearch16() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm lịch sử hai bà trưng";
		assertEquals("websearch" , appClassifier.classify(command));
	}		
}
