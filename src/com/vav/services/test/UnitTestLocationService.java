package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestLocationService {

	@Test
	public void testLocationService1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở gps";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testLocationService2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở  dịch vụ gps";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testLocationService3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tắt dịch vụ gps";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testLocationService4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tắt định vị vị trí";
		assertEquals("settings" , appClassifier.classify(command));
	}		
	@Test
	public void testLocationService5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "khởi động định vị vị trí";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testLocationService6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "định vị vị trí";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testLocationService7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "kích hoạt định vị vị trí";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testLocationService8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "gps";
		assertEquals("settings" , appClassifier.classify(command));
	}	
}
