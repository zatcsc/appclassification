package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestMap {

	@Test
	public void testMap1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở map";
		assertEquals("map" , appClassifier.classify(command));
	}
	@Test
	public void testMap2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "map";
		assertEquals("map" , appClassifier.classify(command));
	}
	@Test
	public void testMap3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở bản đồ";
		assertEquals("map" , appClassifier.classify(command));
	}	
	@Test
	public void testMap4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bản đồ";
		assertEquals("map" , appClassifier.classify(command));
	}		
	@Test
	public void testMap5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "144 xuân thủy ở đâu";
		assertEquals("map" , appClassifier.classify(command));
	}
	@Test
	public void testMap6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "định vị đại học quốc gia hà nội";
		assertEquals("map" , appClassifier.classify(command));
	}
	@Test
	public void testMap7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "có quan ăn nào gần đây không";
		assertEquals("map" , appClassifier.classify(command));
	}
	@Test
	public void testMap8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm các quan ăn gần đây";
		assertEquals("map" , appClassifier.classify(command));
	}
	@Test
	public void testMap9() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chỉ dẫn đường";
		assertEquals("map" , appClassifier.classify(command));
	}	
	@Test
	public void testMap10() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chỉ đường cho tôi đến đại học công nghệ";
		assertEquals("map" , appClassifier.classify(command));
	}	
	@Test
	public void testMap11() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "đến hồ gươm đi như thế nào";
		assertEquals("map" , appClassifier.classify(command));
	}	
	@Test
	public void testMap12() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chỉ đường từ Thái Nguyên đi Hà Nội";
		assertEquals("map" , appClassifier.classify(command));
	}	
	@Test
	public void testMap13() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "cho biết vị trí hiện tại";
		assertEquals("map" , appClassifier.classify(command));
	}	
	@Test
	public void testMap14() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "định vị tòa nhà PVI trần thái tông";
		assertEquals("map" , appClassifier.classify(command));
	}		
	@Test
	public void testMap15() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở bản đồ";
		assertEquals("map" , appClassifier.classify(command));
	}
	@Test
	public void testMap16() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm cây xăng gần nhất";
		assertEquals("map" , appClassifier.classify(command));
	}		
	@Test
	public void testMap17() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm kiếm đường đi";
		assertEquals("map" , appClassifier.classify(command));
	}		
	@Test
	public void testMap18() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm quán ăn";
		assertEquals("map" , appClassifier.classify(command));
	}	
	@Test
	public void testMap19() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm vị trí nhà hàng hoa hoa";
		assertEquals("map" , appClassifier.classify(command));
	}	
	@Test
	public void testMap20() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm xe buýt đi từ đại học công nghệ đến đại học bách khoa";
		assertEquals("map" , appClassifier.classify(command));
	}		
	@Test
	public void testMap21() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tôi muốn biết tôi đang ở đâu";
		assertEquals("map" , appClassifier.classify(command));
	}		
	@Test
	public void testMap22() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tôi muốn đến Hà Nội";
		assertEquals("map" , appClassifier.classify(command));
	}	
	@Test
	public void testMap23() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "từ cầu giấy đi hoàn kiếm thì đi đường nào";
		assertEquals("map" , appClassifier.classify(command));
	}	
	@Test
	public void testMap24() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "từ đây đến đại học quốc gia bao nhiêu cây";
		assertEquals("map" , appClassifier.classify(command));
	}	
	@Test
	public void testMap25() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "vị trí của tôi";
		assertEquals("map" , appClassifier.classify(command));
	}
	@Test
	public void testMap26() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem vị trí của tôi";
		assertEquals("map" , appClassifier.classify(command));
	}		
	@Test
	public void testMap27() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem bản đồ vị trí hiện tại";
		assertEquals("map" , appClassifier.classify(command));
	}		
	@Test
	public void testMap28() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "từ cầu giấy đi hoàn kiếm thì đi đường nào";
		assertEquals("map" , appClassifier.classify(command));
	}		
	@Test
	public void testMap29() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "hiện tại tôi đang ở đâu";
		assertEquals("map" , appClassifier.classify(command));
	}		
	@Test
	public void testMap30() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "Tìm đường đi từ nhà đến chợ gần nhất";
		assertEquals("map" , appClassifier.classify(command));
	}	
	@Test
	public void testMap31() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "Thư viện quốc gia ở đâu";
		assertEquals("map" , appClassifier.classify(command));
	}
	@Test
	public void testMap32() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "Thư viện sư phạm ở đâu";
		assertEquals("map" , appClassifier.classify(command));
	}		
}
