package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestCalendar {

	@Test
	public void testCalendar1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "17 giờ chiều nay đã có cuộc hẹn nào chưa";
		assertEquals("calendar" , appClassifier.classify(command));
	}
	@Test
	public void testCalendar2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "cần gặp những ai hôm nay đây";
		assertEquals("calendar" , appClassifier.classify(command));
	}
	@Test
	public void testCalendar3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "có lịch gì vào ngày thứ 7 tuần này không";
		assertEquals("calendar" , appClassifier.classify(command));
	}	
	@Test
	public void testCalendar4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "có nhắc nhở lịch gì ngày hôm nay không";
		assertEquals("calendar" , appClassifier.classify(command));
	}		
	@Test
	public void testCalendar5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "có việc gì hôm nay không";
		assertEquals("calendar" , appClassifier.classify(command));
	}
	@Test
	public void testCalendar6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "đặt lịch 23 tháng 9 đi tiệc sinh nhật người yêu";
		assertEquals("calendar" , appClassifier.classify(command));
	}
	@Test
	public void testCalendar7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "đặt lịch đi họp vào thứ 5 lúc 8 giờ sáng";
		assertEquals("calendar" , appClassifier.classify(command));
	}
	@Test
	public void testCalendar8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "đặt lịch học";
		assertEquals("calendar" , appClassifier.classify(command));
	}
	@Test
	public void testCalendar9() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "đặt lịch seminar dự án sam sung lúc 13 giờ 30 ngày 5 tháng 2 năm 2015";
		assertEquals("calendar" , appClassifier.classify(command));
	}	
	@Test
	public void testCalendar10() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở lịch";
		assertEquals("calendar" , appClassifier.classify(command));
	}	
	@Test
	public void testCalendar11() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "hẹn đi đón con ở trường";
		assertEquals("calendar" , appClassifier.classify(command));
	}	
	@Test
	public void testCalendar12() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "hẹn gặp Hải trưa thứ 7 tại 144 xuân thủy";
		assertEquals("calendar" , appClassifier.classify(command));
	}	
	@Test
	public void testCalendar13() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "hiện tại còn rảnh lúc nào";
		assertEquals("calendar" , appClassifier.classify(command));
	}	
	@Test
	public void testCalendar14() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "cuối tuần này có lịch gì không";
		assertEquals("calendar" , appClassifier.classify(command));
	}		
	@Test
	public void testCalendar15() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "hôm nay có lịch công việc nào không";
		assertEquals("calendar" , appClassifier.classify(command));
	}
	@Test
	public void testCalendar16() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "hôm nay là thứ mấy";
		assertEquals("calendar" , appClassifier.classify(command));
	}		
	@Test
	public void testCalendar17() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "hôm nay là ngày gì";
		assertEquals("calendar" , appClassifier.classify(command));
	}		
	@Test
	public void testCalendar18() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "hủy tất cả các lịch ngày mai";
		assertEquals("calendar" , appClassifier.classify(command));
	}	
	@Test
	public void testCalendar19() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "kế hoạch hôm nay là gì";
		assertEquals("calendar" , appClassifier.classify(command));
	}	
	@Test
	public void testCalendar20() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "kiểm tra lịch ngày mai cho tôi";
		assertEquals("calendar" , appClassifier.classify(command));
	}		
	@Test
	public void testCalendar21() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "lịch hôm nay đã full chưa";
		assertEquals("calendar" , appClassifier.classify(command));
	}		
	@Test
	public void testCalendar22() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "lịch ngày kia đã full chưa";
		assertEquals("calendar" , appClassifier.classify(command));
	}	
	@Test
	public void testCalendar23() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "lịch hôm nay giống hôm qua";
		assertEquals("calendar" , appClassifier.classify(command));
	}	
	@Test
	public void testCalendar24() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tuần này có sự kiện gì không";
		assertEquals("calendar" , appClassifier.classify(command));
	}	
	@Test
	public void testCalendar25() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem lịch học";
		assertEquals("calendar" , appClassifier.classify(command));
	}		
	@Test
	public void testCalendar26() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "lịch âm";
		assertEquals("calendar" , appClassifier.classify(command));
	}	
	@Test
	public void testCalendar27() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "âm lịch hôm nay là bao nhiêu";
		assertEquals("calendar" , appClassifier.classify(command));
	}	
	@Test
	public void testCalendar28() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem ngày âm lịch hôm nay";
		assertEquals("calendar" , appClassifier.classify(command));
	}	
	@Test
	public void testCalendar29() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "ngày rằm sắp tới là thứ mấy";
		assertEquals("calendar" , appClassifier.classify(command));
	}	
}
