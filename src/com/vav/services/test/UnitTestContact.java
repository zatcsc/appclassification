package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestContact {

	@Test
	public void testContact1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chỉnh sửa thông tin trong danh bạ";
		assertEquals("contact" , appClassifier.classify(command));
	}
	@Test
	public void testContact2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm thông tin contact của anh Nam";
		assertEquals("contact" , appClassifier.classify(command));
	}
	@Test
	public void testContact3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem danh bạ của anh Nam";
		assertEquals("contact" , appClassifier.classify(command));
	}	
	@Test
	public void testContact4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở contact";
		assertEquals("contact" , appClassifier.classify(command));
	}		
	@Test
	public void testContact5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "contact";
		assertEquals("contact" , appClassifier.classify(command));
	}
	@Test
	public void testContact6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chuyển danh bạ sang máy";
		assertEquals("contact" , appClassifier.classify(command));
	}
	@Test
	public void testContact7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "cập nhật danh bạ";
		assertEquals("contact" , appClassifier.classify(command));
	}
	@Test
	public void testContact8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem danh bạ";
		assertEquals("contact" , appClassifier.classify(command));
	}
	@Test
	public void testContact9() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "cập nhật danh bạ từ tài khoản google";
		assertEquals("contact" , appClassifier.classify(command));
	}	
	@Test
	public void testContact10() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "có số của Hợp trong danh bạ chưa";
		assertEquals("contact" , appClassifier.classify(command));
	}	
	@Test
	public void testContact11() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "lưu số điện thoại mới";
		assertEquals("contact" , appClassifier.classify(command));
	}	
	@Test
	public void testContact12() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "lưu số máy vừa gọi";
		assertEquals("contact" , appClassifier.classify(command));
	}	
	@Test
	public void testContact13() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "sửa số liên lạc của Hợp";
		assertEquals("contact" , appClassifier.classify(command));
	}	
	@Test
	public void testContact14() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "thêm số vừa gọi đến vào danh bạ của Yến";
		assertEquals("contact" , appClassifier.classify(command));
	}		
	@Test
	public void testContact15() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "thêm danh bạ";
		assertEquals("contact" , appClassifier.classify(command));
	}
	@Test
	public void testContact16() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "lưu số 900 vào danh bạ";
		assertEquals("contact" , appClassifier.classify(command));
	}		
	@Test
	public void testContact17() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "lưu số của anh Thanh là 0913942750";
		assertEquals("contact" , appClassifier.classify(command));
	}		
	@Test
	public void testContact18() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "thêm số điện thoại vào nhóm bạn bè";
		assertEquals("contact" , appClassifier.classify(command));
	}	
	@Test
	public void testContact19() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm cho tôi số điện thoại của mẹ";
		assertEquals("contact" , appClassifier.classify(command));
	}	
	@Test
	public void testContact20() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm contact của Hồng";
		assertEquals("contact" , appClassifier.classify(command));
	}		
	@Test
	public void testContact21() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "số điện thoại của Luân là gì vậy";
		assertEquals("contact" , appClassifier.classify(command));
	}		
	@Test
	public void testContact22() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "Tìm số của Ngọc";
		assertEquals("contact" , appClassifier.classify(command));
	}	
	@Test
	public void testContact23() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem thông tin của Hợp";
		assertEquals("contact" , appClassifier.classify(command));
	}	
	@Test
	public void testContact24() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xóa số vừa gọi";
		assertEquals("contact" , appClassifier.classify(command));
	}	
	@Test
	public void testContact25() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xóa số của Hoàng";
		assertEquals("contact" , appClassifier.classify(command));
	}		
	@Test
	public void testContact26() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tra danh bạ";
		assertEquals("contact" , appClassifier.classify(command));
	}	
	@Test
	public void testContact27() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tìm số điện thoại của gấu";
		assertEquals("contact" , appClassifier.classify(command));
	}	
	@Test
	public void testContact28() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "thêm số 01656077594 vào danh bạ lưu là Hợp";
		assertEquals("contact" , appClassifier.classify(command));
	}	
	@Test
	public void testContact29() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tạo nhóm liên lạc";
		assertEquals("contact" , appClassifier.classify(command));
	}	
}
