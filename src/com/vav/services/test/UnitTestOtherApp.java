package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestOtherApp {

	@Test
	public void testOtherApp1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bật chat zalo";
		assertEquals("otherapp" , appClassifier.classify(command));
	}
	@Test
	public void testOtherApp2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở chat zalo";
		assertEquals("otherapp" , appClassifier.classify(command));
	}
	@Test
	public void testOtherApp3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bật drive";
		assertEquals("otherapp" , appClassifier.classify(command));
	}	
	@Test
	public void testOtherApp4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bật dropbox";
		assertEquals("otherapp" , appClassifier.classify(command));
	}		
	@Test
	public void testOtherApp5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "cập nhật ứng dụng đã có";
		assertEquals("otherapp" , appClassifier.classify(command));
	}
	@Test
	public void testOtherApp6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chơi cờ caro";
		assertEquals("otherapp" , appClassifier.classify(command));
	}
	@Test
	public void testOtherApp7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chơi tiến lên miền bắc";
		assertEquals("otherapp" , appClassifier.classify(command));
	}
	@Test
	public void testOtherApp8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chơi phỏm";
		assertEquals("otherapp" , appClassifier.classify(command));
	}
	@Test
	public void testOtherApp9() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chơi đào vàng";
		assertEquals("otherapp" , appClassifier.classify(command));
	}	
	@Test
	public void testOtherApp13() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tắt candy crush saga";
		assertEquals("otherapp" , appClassifier.classify(command));
	}	
	@Test
	public void testOtherApp14() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở trò chơi Candy crush saga";
		assertEquals("otherapp" , appClassifier.classify(command));
	}		
	@Test
	public void testOtherApp15() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở trò chơi đào vàng";
		assertEquals("otherapp" , appClassifier.classify(command));
	}
	@Test
	public void testOtherApp16() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tắt messenger";
		assertEquals("otherapp" , appClassifier.classify(command));
	}		
	@Test
	public void testOtherApp17() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở ứng dụng quora";
		assertEquals("otherapp" , appClassifier.classify(command));
	}		
	@Test
	public void testOtherApp18() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở dropbox";
		assertEquals("otherapp" , appClassifier.classify(command));
	}	
	@Test
	public void testOtherApp19() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở googlen drive";
		assertEquals("otherapp" , appClassifier.classify(command));
	}	
	@Test
	public void testOtherApp20() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở instagram";
		assertEquals("otherapp" , appClassifier.classify(command));
	}		
	@Test
	public void testOtherApp21() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở quora";
		assertEquals("otherapp" , appClassifier.classify(command));
	}		
	@Test
	public void testOtherApp22() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở hay day";
		assertEquals("otherapp" , appClassifier.classify(command));
	}	
	@Test
	public void testOtherApp23() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở từ điển";
		assertEquals("otherapp" , appClassifier.classify(command));
	}						
	@Test
	public void testOtherApp28() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chạy ứng dụng viber";
		assertEquals("otherapp" , appClassifier.classify(command));
	}		
	@Test
	public void testOtherApp29() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "zalo có tin nhắn mới không";
		assertEquals("otherapp" , appClassifier.classify(command));
	}		
	@Test
	public void testOtherApp30() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chơi game đuổi hình bắt chữ";
		assertEquals("otherapp" , appClassifier.classify(command));
	}		
}
