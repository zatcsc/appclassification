package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestSMS {

	@Test
	public void testSMS1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở tin nhắn hôm nay";
		assertEquals("sms" , appClassifier.classify(command));
	}
	@Test
	public void testSMS2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "có tin nhắn mới nào không";
		assertEquals("sms" , appClassifier.classify(command));
	}
	@Test
	public void testSMS3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "đọc tin nhắn";
		assertEquals("sms" , appClassifier.classify(command));
	}	
	@Test
	public void testSMS4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tin nhắn đến";
		assertEquals("sms" , appClassifier.classify(command));
	}		
	@Test
	public void testSMS5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "gửi tin nhắn tôi đang bận";
		assertEquals("sms" , appClassifier.classify(command));
	}
	@Test
	public void testSMS6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "Gửi tin nhắn cho mai";
		assertEquals("sms" , appClassifier.classify(command));
	}
	@Test
	public void testSMS7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "nhắn tin cho Dương";
		assertEquals("sms" , appClassifier.classify(command));
	}
	@Test
	public void testSMS8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "nhắn tin cho gấu";
		assertEquals("sms" , appClassifier.classify(command));
	}
	@Test
	public void testSMS9() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tôi muốn gửi tin nhắn về nhà muộn cho mẹ";
		assertEquals("sms" , appClassifier.classify(command));
	}	
	@Test
	public void testSMS10() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "nhắn tin chúc mừng sinh nhật cho Minh";
		assertEquals("sms" , appClassifier.classify(command));
	}	
	@Test
	public void testSMS11() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "ghi nội dung tin nhắn là hôm nay tớ không đến chỗ hẹn được vào tin nhắn mới";
		assertEquals("sms" , appClassifier.classify(command));
	}	
	@Test
	public void testSMS12() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "Tôi muốn nhắn tin cho ông Chí";
		assertEquals("sms" , appClassifier.classify(command));
	}	
	@Test
	public void testSMS13() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "nhắn tin tới số 0975116875";
		assertEquals("sms" , appClassifier.classify(command));
	}	
	@Test
	public void testSMS14() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "Tìm các tin nhắn của mai";
		assertEquals("sms" , appClassifier.classify(command));
	}		
	@Test
	public void testSMS15() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem tất cả tin nhắn của Vũ";
		assertEquals("sms" , appClassifier.classify(command));
	}
	@Test
	public void testSMS16() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "gửi tin nhắn tôi đang bận đến số 0912152965";
		assertEquals("sms" , appClassifier.classify(command));
	}		
	@Test
	public void testSMS17() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "có tin nhắn mới nào không";
		assertEquals("sms" , appClassifier.classify(command));
	}		
	@Test
	public void testSMS18() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "gửi tin nhắn đến tất cả mọi người trong danh bạ";
		assertEquals("sms" , appClassifier.classify(command));
	}		
}
