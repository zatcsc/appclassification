package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestBrowser {

	@Test
	public void testBrowser1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "lên bóng đá trực tuyến chấm vn";
		assertEquals("browser" , appClassifier.classify(command));
	}
	@Test
	public void testBrowserDanTri() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();
		
		String command = "dantri.com";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "dân trí";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "vào dân trí";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "vào dantri.com";
		assertEquals("browser" , appClassifier.classify(command));

		command = "mở dân trí";
		assertEquals("browser" , appClassifier.classify(command));

		command = "dân trí chấm com";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "dân trí chấm com chấm vn";
		assertEquals("browser" , appClassifier.classify(command));		
	
		command = "báo dân trí";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "đọc báo dân trí";
		assertEquals("browser" , appClassifier.classify(command));		
	}
	
	@Test
	public void testBrowserBaoMoi() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "báo mới";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "baomoi.com";
		assertEquals("browser" , appClassifier.classify(command));

		command = "mở báo mới";
		assertEquals("browser" , appClassifier.classify(command));		
		
		command = "báo mới chấm com";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "báo mới chấm com chấm vn";
		assertEquals("browser" , appClassifier.classify(command));

		command = "vào báo mới";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "đọc báo mới";
		assertEquals("browser" , appClassifier.classify(command));		
	}	
	@Test
	public void testBrowserVnExpress() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "vnexpress";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "vnexpress.net";
		assertEquals("browser" , appClassifier.classify(command));		
		
		command = "vnexpress chấm nét";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "vào vnexpress";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "mở vnexpress";
		assertEquals("browser" , appClassifier.classify(command));

		command = "đọc báo vnexpress";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "đọc vnexpress";
		assertEquals("browser" , appClassifier.classify(command));		
	}	
	
	@Test
	public void testBrowserTinhTe() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tinh tế";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "tinh tế chấm vn";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "tinhte.vn";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "vào diễn đàn tinh tế";
		assertEquals("browser" , appClassifier.classify(command));

		command = "đọc báo tinh tế";
		assertEquals("browser" , appClassifier.classify(command));
		
	}	
	@Test
	public void testBrowserOtherMagazine(){
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();
		
		String command = "zing chấm vn";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "zing";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "báo pháp luật";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "báo thanh niên";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "báo pháp luật đời sống";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "web trẻ thơ";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "diễn đàn web trẻ thơ";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "webtretho.com";
		assertEquals("browser" , appClassifier.classify(command));		
		
	}

	@Test
	public void testBrowserTuoiTre() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tuoitre.vn";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "báo tuổi trẻ";
		assertEquals("browser" , appClassifier.classify(command));		
		
		command = "đọc báo tuổi trẻ";
		assertEquals("browser" , appClassifier.classify(command));
		
		command = "mở báo tuổi trẻ chấm vn";
		assertEquals("browser" , appClassifier.classify(command));				
	}			
		
		
				
	@Test
	public void testBrowser3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "lướt web";
		assertEquals("browser" , appClassifier.classify(command));
	}	
	@Test
	public void testBrowser4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "truy cập internet";
		assertEquals("browser" , appClassifier.classify(command));
	}		
	@Test
	public void testBrowser5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "vào trang chủ của google";
		assertEquals("browser" , appClassifier.classify(command));
	}
	@Test
	public void testBrowser6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "vào web hai tư giờ chấm com";
		assertEquals("browser" , appClassifier.classify(command));
	}
	@Test
	public void testBrowser7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "vào trình duyệt";
		assertEquals("browser" , appClassifier.classify(command));
	}
	@Test
	public void testBrowser8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "vào google";
		assertEquals("browser" , appClassifier.classify(command));				
	}
	@Test
	public void testBrowser9() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "vào play.fpt.vn";
		assertEquals("browser" , appClassifier.classify(command));		
		command = "vào báo điện tử dân trí";
		assertEquals("browser" , appClassifier.classify(command));		
	}	
}
