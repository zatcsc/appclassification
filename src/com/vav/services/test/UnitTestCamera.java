package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestCamera {

	@Test
	public void testCamera1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở chế độ chụp ảnh selfie nào";
		assertEquals("photo" , appClassifier.classify(command));
	}
	@Test
	public void testCamera2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "selfie nào";
		assertEquals("photo" , appClassifier.classify(command));
	}
	@Test
	public void testCamera3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bật máy ảnh";
		assertEquals("photo" , appClassifier.classify(command));
	}	
	@Test
	public void testCamera4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chụp cho mình một kiểu đi";
		assertEquals("photo" , appClassifier.classify(command));
	}		
	@Test
	public void testCamera5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chụp hình nào";
		assertEquals("photo" , appClassifier.classify(command));
	}
	@Test
	public void testCamera6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở phần mềm quay phim";
		assertEquals("video" , appClassifier.classify(command));
	}
	@Test
	public void testCamera7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chọn chế độ quay phim";
		assertEquals("video" , appClassifier.classify(command));
	}
	@Test
	public void testCamera8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chọn trình chụp ảnh";
		assertEquals("photo" , appClassifier.classify(command));
	}
	@Test
	public void testCamera9() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chụp";
		assertEquals("photo" , appClassifier.classify(command));
	}	
	@Test
	public void testCamera10() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chụp ảnh tự động";
		assertEquals("photo" , appClassifier.classify(command));
	}	
	@Test
	public void testCamera11() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chụp lại khoảnh khắc này";
		assertEquals("photo" , appClassifier.classify(command));
	}	
	@Test
	public void testCamera12() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "ghi lại khoảnh khắc này";
		assertEquals("video" , appClassifier.classify(command));
	}	
	@Test
	public void testCamera13() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở camera cho tôi";
		assertEquals("photo" , appClassifier.classify(command));
	}	
	@Test
	public void testCamera14() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "quay video";
		assertEquals("video" , appClassifier.classify(command));
	}		
	@Test
	public void testCamera15() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở camera để selfie nào";
		assertEquals("photo" , appClassifier.classify(command));
	}
	@Test
	public void testCamera16() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở máy chụp hình";
		assertEquals("photo" , appClassifier.classify(command));
	}		
	@Test
	public void testCamera17() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở phần mềm chụp hình";
		assertEquals("photo" , appClassifier.classify(command));
	}		
	@Test
	public void testCamera18() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở máy ảnh trước";
		assertEquals("photo" , appClassifier.classify(command));
	}	
	@Test
	public void testCamera19() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chụp hình tự sướng";
		assertEquals("photo" , appClassifier.classify(command));
	}	
	@Test
	public void testCamera20() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "quay phim bằng camera trước";
		assertEquals("video" , appClassifier.classify(command));
	}		
	@Test
	public void testCamera21() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bật camera trước lên";
		assertEquals("photo" , appClassifier.classify(command));
	}		
	@Test
	public void testCamera22() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bật camera để chụp ảnh nào";
		assertEquals("photo" , appClassifier.classify(command));
	}		
}
