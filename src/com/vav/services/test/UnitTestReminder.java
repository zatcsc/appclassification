package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestReminder {

	@Test
	public void testReminder1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "nhắc nhở";
		assertEquals("reminder" , appClassifier.classify(command));
	}
	@Test
	public void testReminder2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem nhắc nhở";
		assertEquals("reminder" , appClassifier.classify(command));
	}
	@Test
	public void testReminder3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "nhắc tôi mùa quà sinh nhật cho mẹ";
		assertEquals("reminder" , appClassifier.classify(command));
	}	
	@Test
	public void testReminder4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "nhắc tôi uống thuốc sau 2 giờ nữa";
		assertEquals("reminder" , appClassifier.classify(command));
	}		
	@Test
	public void testReminder5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xóa nhắc nhở uống thuốc";
		assertEquals("reminder" , appClassifier.classify(command));
	}
	@Test
	public void testReminder6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tắt nhắc nhở sinh nhật tôi";
		assertEquals("reminder" , appClassifier.classify(command));
	}
	@Test
	public void testReminder7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "nhắc tôi uống thuốc lúc 8 giờ nhé";
		assertEquals("reminder" , appClassifier.classify(command));
	}
	@Test
	public void testReminder8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "nhắc tôi tắt chế độ im lặng sau buổi học";
		assertEquals("reminder" , appClassifier.classify(command));
	}
	@Test
	public void testReminder9() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "nhắc tôi tặng hoa";
		assertEquals("reminder" , appClassifier.classify(command));
	}	
	@Test
	public void testReminder10() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "nhắc tôi giờ họp";
		assertEquals("reminder" , appClassifier.classify(command));
	}	
	@Test
	public void testReminder11() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "nhắc nhở ngày kiểm tra định kỳ sức khoẻ hàng tháng";
		assertEquals("reminder" , appClassifier.classify(command));
	}	
	@Test
	public void testReminder12() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "nhắc nhở các ngày lễ trong năm";
		assertEquals("reminder" , appClassifier.classify(command));
	}	
	@Test
	public void testReminder13() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "nhắc lịch dạy lúc 10 giờ";
		assertEquals("reminder" , appClassifier.classify(command));
	}	
	@Test
	public void testReminder14() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "nhắc giờ ăn trưa";
		assertEquals("reminder" , appClassifier.classify(command));
	}		
	@Test
	public void testReminder15() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "ngừng báo nhắc nhở họp ngày mai";
		assertEquals("reminder" , appClassifier.classify(command));
	}
	@Test
	public void testReminder16() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "nhắc nhở kỉ niệm ngày cưới";
		assertEquals("reminder" , appClassifier.classify(command));
	}		
	@Test
	public void testReminder17() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mai có sinh nhật của ai không";
		assertEquals("reminder" , appClassifier.classify(command));
	}		
	@Test
	public void testReminder18() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "đặt thông báo 23 tháng 9 sinh nhật người yêu";
		assertEquals("reminder" , appClassifier.classify(command));
	}	
	@Test
	public void testReminder19() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "đặt nhắc nhở 9h sáng mai";
		assertEquals("reminder" , appClassifier.classify(command));
	}	
	@Test
	public void testReminder20() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "5h nhắc tôi về đi tập thể dục";
		assertEquals("reminder" , appClassifier.classify(command));
	}		
	@Test
	public void testReminder21() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "11h45 nhắc tôi đi ăn";
		assertEquals("reminder" , appClassifier.classify(command));
	}		
	@Test
	public void testReminder22() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "kiểm tra nhắc nhở";
		assertEquals("reminder" , appClassifier.classify(command));
	}	
	@Test
	public void testReminder23() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "ngừng báo nhắc nhở họp ngày mai";
		assertEquals("reminder" , appClassifier.classify(command));
	}		
}
