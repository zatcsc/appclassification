package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestSetting {

	@Test
	public void testSetting1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "cài đặt";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSetting2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "cài đặt hiển thị";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSetting3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "chọn cài đặt";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSetting4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "vào setting";
		assertEquals("settings" , appClassifier.classify(command));
	}		
	@Test
	public void testSetting5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở cài đặt điện thoại";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSetting6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "cài đặt hiển thị";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSetting7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "thiết lập bảo mật";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSetting8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "cài đặt cấu hình";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSetting9() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tăng kích thước chữ";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSetting10() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tăng kích thước chữ";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSetting11() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "cài đặt cỡ chữ to nhất";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSetting12() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "thiết lập điện thoại";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSetting13() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở thiết lập điện thoại ";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSetting14() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "cài đặt hình nền";
		assertEquals("settings" , appClassifier.classify(command));
	}		
	@Test
	public void testSetting15() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "vào chế độ máy bay";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSetting16() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "cài đặt phụ kiện";
		assertEquals("settings" , appClassifier.classify(command));
	}		
	@Test
	public void testSetting17() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "cài đặt font chữ";
		assertEquals("settings" , appClassifier.classify(command));
	}		
	@Test
	public void testSetting18() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "thiết lập thời gian";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSetting19() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "thiết lập múi giờ";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSetting20() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "giảm kích thước chữ";
		assertEquals("settings" , appClassifier.classify(command));
	}				
}
