package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestPhone {

	@Test
	public void testPhone1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "điện thoại";
		assertEquals("phone" , appClassifier.classify(command));
	}
	@Test
	public void testPhone2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "gọi điện";
		assertEquals("phone" , appClassifier.classify(command));
	}
	@Test
	public void testPhone3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "quay số";
		assertEquals("phone" , appClassifier.classify(command));
	}	
	@Test
	public void testPhone4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "phone";
		assertEquals("phone" , appClassifier.classify(command));
	}		
	@Test
	public void testPhone5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "call 06334569812";
		assertEquals("phone" , appClassifier.classify(command));
	}
	@Test
	public void testPhone6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "gọi 0363511345";
		assertEquals("phone" , appClassifier.classify(command));
	}
	@Test
	public void testPhone7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "gọi Taxi Mai Minh";
		assertEquals("phone" , appClassifier.classify(command));
	}
	@Test
	public void testPhone8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "kiểm tra tài khoản của tôi";
		assertEquals("phone" , appClassifier.classify(command));
	}
	@Test
	public void testPhone9() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "Gọi cho Mai";
		assertEquals("phone" , appClassifier.classify(command));
	}	
	@Test
	public void testPhone10() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "gọi điện";
		assertEquals("phone" , appClassifier.classify(command));
	}	
	@Test
	public void testPhone11() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "gọi điện thoại cho gấu";
		assertEquals("phone" , appClassifier.classify(command));
	}	
	@Test
	public void testPhone12() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "điện thoại cho bố";
		assertEquals("phone" , appClassifier.classify(command));
	}	
	@Test
	public void testPhone13() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "điện thoại cho vợ yêu";
		assertEquals("phone" , appClassifier.classify(command));
	}	
	@Test
	public void testPhone14() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "gọi mai";
		assertEquals("phone" , appClassifier.classify(command));
	}		
	@Test
	public void testPhone15() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "gọi số 0230123456";
		assertEquals("phone" , appClassifier.classify(command));
	}
	@Test
	public void testPhone16() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở gọi điện";
		assertEquals("phone" , appClassifier.classify(command));
	}		
	@Test
	public void testPhone17() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "quay số 02202561262";
		assertEquals("phone" , appClassifier.classify(command));
	}		
	@Test
	public void testPhone18() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "gọi thoại cho chị ngọc";
		assertEquals("phone" , appClassifier.classify(command));
	}	
	@Test
	public void testPhone19() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "điện cho Tuấn";
		assertEquals("phone" , appClassifier.classify(command));
	}	
	@Test
	public void testPhone20() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "alo cho cậu cái";
		assertEquals("phone" , appClassifier.classify(command));
	}		
	@Test
	public void testPhone21() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "alo cho bố ";
		assertEquals("phone" , appClassifier.classify(command));
	}		
	@Test
	public void testPhone22() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "gọi cứu hỏa";
		assertEquals("phone" , appClassifier.classify(command));
	}	
	@Test
	public void testPhone23() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "điện thoại cho mẹ";
		assertEquals("phone" , appClassifier.classify(command));
	}	
	@Test
	public void testPhone24() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "phone cho mẹ";
		assertEquals("phone" , appClassifier.classify(command));
	}	
	@Test
	public void testPhone25() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tài khoản còn lại bao nhiêu";
		assertEquals("phone" , appClassifier.classify(command));
	}
	@Test
	public void testPhone26() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "phone cho người yêu cái nào";
		assertEquals("phone" , appClassifier.classify(command));
	}		
	@Test
	public void testPhone27() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "phone thằng bạn chém gió cái đã";
		assertEquals("phone" , appClassifier.classify(command));
	}		
	@Test
	public void testPhone28() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "còn bao nhiêu tiền trong tài khoản";
		assertEquals("phone" , appClassifier.classify(command));
	}		
	@Test
	public void testPhone29() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "vào phần gọi điện";
		assertEquals("phone" , appClassifier.classify(command));
	}		
	@Test
	public void testPhone30() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "gọi thoại cho chị ngọc";
		assertEquals("phone" , appClassifier.classify(command));
	}		
}
