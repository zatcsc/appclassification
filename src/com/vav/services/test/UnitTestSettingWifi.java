package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestSettingWifi {

	@Test
	public void testSettingWifi1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tắt wifi";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingWifi2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bật wifi";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingWifi3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tự kết nối wifi";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSettingWifi4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "kích hoạt wifi";
		assertEquals("settings" , appClassifier.classify(command));
	}		
	@Test
	public void testSettingWifi5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở cài đặt wifi";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingWifi6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bật wifi lên nào";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingWifi7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "ở đây có wifi không";
		assertEquals("settings" , appClassifier.classify(command));
	}	
}
