package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestWeather {

	@Test
	public void testWeather1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "thời tiết ở hà nội ngày mai thế nào";
		assertEquals("weather" , appClassifier.classify(command));
	}
	@Test
	public void testWeather2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "hôm nay bao nhiêu độ?";
		assertEquals("weather" , appClassifier.classify(command));
	}
	@Test
	public void testWeather3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "hôm nay mưa hay nắng";
		assertEquals("weather" , appClassifier.classify(command));
	}	
	@Test
	public void testWeather4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem thời tiết hiện tại";
		assertEquals("weather" , appClassifier.classify(command));
	}		
	@Test
	public void testWeather5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem dự báo thời tiết 3 ngày tới";
		assertEquals("weather" , appClassifier.classify(command));
	}
	@Test
	public void testWeather6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "thời tiết ngày mai như thế nào";
		assertEquals("weather" , appClassifier.classify(command));
	}
	@Test
	public void testWeather7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "ngoài trời có mưa không";
		assertEquals("weather" , appClassifier.classify(command));
	}
	@Test
	public void testWeather8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "dự báo thời tiết vào 6 giờ hàng ngày";
		assertEquals("weather" , appClassifier.classify(command));
	}
	@Test
	public void testWeather9() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem thời tiết hoà bình ngày mai";
		assertEquals("weather" , appClassifier.classify(command));
	}	
	@Test
	public void testWeather10() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "trời nồm quá độ ẩm là bao nhiêu";
		assertEquals("weather" , appClassifier.classify(command));
	}	
	@Test
	public void testWeather11() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "Quảng Bình nắng như thế nào";
		assertEquals("weather" , appClassifier.classify(command));
	}	
	@Test
	public void testWeather12() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem thông tin về cơn bão trên biển đông";
		assertEquals("weather" , appClassifier.classify(command));
	}	
	@Test
	public void testWeather13() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "cập nhật thông tin về cơn bão hiện tại";
		assertEquals("weather" , appClassifier.classify(command));
	}	
	@Test
	public void testWeather14() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "xem thời tiết 3 ngày tới";
		assertEquals("weather" , appClassifier.classify(command));
	}		
	@Test
	public void testWeather15() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "nhiệt độ trong nhà hiện tại là bao nhiêu";
		assertEquals("weather" , appClassifier.classify(command));
	}	
}
