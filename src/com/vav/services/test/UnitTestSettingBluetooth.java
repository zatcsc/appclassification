package com.vav.services.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vav.services.featureclassification.AppClassifier;

public class UnitTestSettingBluetooth {

	@Test
	public void testSettingBluetooth1() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bật bluetooth";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingBluetooth2() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "kết nối bluetooth";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingBluetooth3() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "ngắt kết nối bluetooth";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSettingBluetooth4() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "ngừng bluetooth";
		assertEquals("settings" , appClassifier.classify(command));
	}		
	@Test
	public void testSettingBluetooth5() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "khởi động bluetooth";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingBluetooth6() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "tạm dừng bluetooth";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingBluetooth7() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "khởi động bluetooth";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingBluetooth8() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bật dịch vụ bluetooth";
		assertEquals("settings" , appClassifier.classify(command));
	}
	@Test
	public void testSettingBluetooth9() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "bluetooth";
		assertEquals("settings" , appClassifier.classify(command));
	}	
	@Test
	public void testSettingBluetooth10() {
		AppClassifier appClassifier = new AppClassifier("model");
		appClassifier.init();		
		String command = "mở cài đặt bluetooth";
		assertEquals("settings" , appClassifier.classify(command));
	}	
}
