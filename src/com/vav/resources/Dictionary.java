package com.vav.resources;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import jmdn.base.util.string.StrUtil;

public class Dictionary {
	private String dictPath;
	private Set<String> entries;

	public Dictionary(String dictPath) {
		this.dictPath = dictPath;
		entries = new HashSet<>();
	}

	private boolean loadDict(String dictPath){
		BufferedReader br;
		String line;
		try{
			br = new BufferedReader(new FileReader(new File(dictPath)));
			while ((line = br.readLine())!= null){
				entries.add(StrUtil.normalizeString(line));
			}
		} catch(IOException e){
			e.printStackTrace();
			System.err.println("File không tồn tại");
			return false;
		}
		return true;
	}
	private boolean contains(String entry){
		return entries.contains(StrUtil.normalizeString(entry));
	}
}
